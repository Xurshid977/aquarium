import javax.swing.*;

public class Main {

    public static void main(String[] args) {
        if (JOptionPane.showConfirmDialog(null, "Boshlash", "start", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) == JOptionPane.YES_OPTION)
            Life.getInstance().run();
    }
}
