import java.util.ArrayList;
import java.util.List;

public class Akvarium {
    private List<Fish> fishList;
    private static Akvarium ourInstance = new Akvarium();

    public static Akvarium getInstance() {
        return ourInstance;
    }

    private Akvarium() {
    }

    public void setFishList(List<Fish> fishList) {
        this.fishList = fishList;
    }

    public List<Fish> getFishList() {
        return fishList;
    }

    public void addFishToAkvarium(List<Fish> fishList) {
        int fishesSize = this.fishList.size() + fishList.size();
        if (fishesSize > getCount()) {
            System.out.println("Akvariumda " + this.fishList.size() + " ta baliq bor.");
            System.out.println("Akvariumga "+fishList.size()+" ta baliq qo'shib bo'lmaydi.");
            System.exit(0);
        }
        List<Fish> fishes = new ArrayList<>(this.fishList);
        fishes.addAll(fishList);
        this.fishList = fishes;
        fishList.forEach(fish -> {
            System.out.println(fish.getName() + " nomli baliq tug'ildi.");
            System.out.println("Jinsi: " + fish.getGender().name());
        });
        System.out.println(fishList.size()+" ta baliq tug'ildi");
        System.out.println("Akvariumda " + this.fishList.size() + " ta baliq bor.");
    }

    private Integer getCount() {
        return 1000;
    }
}



