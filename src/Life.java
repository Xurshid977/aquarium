import java.util.*;

public class Life implements Runnable {
    private static Life ourInstance = new Life();

    public static Life getInstance() {
        return ourInstance;
    }

    private Life() {
    }

    private void bornFishes() {
        Random random = new Random();
        int x = random.nextInt(20);
        List<Fish> fishList = new ArrayList<>();
        for (int i = 0; i < x; i++) {
            int gender = random.nextInt(2);
            fishList.add(new Fish(UUID.randomUUID().toString(),
                    gender == 0 ? GenderEnum.MALE : GenderEnum.FEMALE));
        }
        Akvarium.getInstance().addFishToAkvarium(fishList);
    }

    private void deadFishes() {
        Akvarium akvarium = Akvarium.getInstance();
        Random random = new Random();
        int x = random.nextInt(10);
        if (akvarium.getFishList().size() - 2 < x) {
            System.out.println(x + " ta baliqni o'ldirishni iloji yo'q chunki akvariumda baliqlar kam qoladi.");
            return;
        }
        for (int i = 0; i < x; i++) {
            Fish fish = akvarium.getFishList().get(random.nextInt(akvarium.getFishList().size()));
            System.out.println(fish.getName() + " nomli baliq o'ldi.");
            System.out.println("Yoshi: " + fish.getAge());
            akvarium.getFishList().remove(fish);
        }
        System.out.println(x + " ta baliq o'ldi.");
    }

    @Override
    public void run() {
        Life life = ourInstance;
        Fish ona = new Fish("fish1", GenderEnum.FEMALE);
        Fish ota = new Fish("fish2", GenderEnum.MALE);
        Akvarium akvarium = Akvarium.getInstance();
        akvarium.setFishList(Arrays.asList(ota, ona));
        int t = 0;
        int deadTime = new Random().nextInt(5) + 5;
        while (true) {
            life.bornFishes();
            if (t > deadTime) {
                life.deadFishes();
//                System.out.println("Vaqt: " + t);
            }
            t++;
            akvarium.getFishList().forEach(Fish::addAge);
        }
    }
}
